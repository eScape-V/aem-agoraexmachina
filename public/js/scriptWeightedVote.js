

//On efface tout les formulaires pour ne montrer que le select du votant
function initialise(idProposal){
    let selection = document.getElementById('selection' + idProposal);
    let articles = document.getElementsByTagName('article');

    for(let article of articles){
        article.setAttribute('style', 'display:none;');
    }
        //A chaque changement de selection, on efface tout les formulaires pour n'afficher que celui qui correspond
    selection.onchange = function (){
        for(let article of articles){
            article.setAttribute('style', 'display:none;');
        }

        //On regarde au nom de qui on vote
        let idSelection = selection.value;
        let articleAAfficher = document.getElementById('VoteDelegation' + idSelection + '' + idProposal );
        //Si on a deja voté, pour nous ou en délégation, on enlève l'input de points
        let condition = !!document.getElementById('divEffacer' + idProposal + '' + idSelection);
        if(condition){
            cacherInput(idProposal, idSelection);
        }

        //On affiche l'input correspondant
        articleAAfficher.setAttribute('style', 'display:initial;');
        let inputDelegation = document.getElementById('selectPoints'+idProposal+''+idSelection);

        //mise à jour des points restant à voter
        let pointADonnerDelegation = document.getElementById('pointsADonner'+idSelection).value;
        inputDelegation.setAttribute('max', pointADonnerDelegation);
        let pMax = document.getElementById('pMaxDelegation'+idSelection + '' + idProposal);
        pMax.innerText = pointADonnerDelegation;
    }
}

function cacherInput(idProposal, idSelection){
    //On enleve l'input de points du DOM
    let inputNumber = document.getElementById('selectPoints'+  idProposal + '' +  idSelection);
    let labelCorrespondant = inputNumber.previousSibling;
    inputNumber.setAttribute('style', 'display:none;');
    labelCorrespondant.setAttribute('style', 'display:none;');
}



