# Installation de la plateforme de démocratie liquide Agora Ex Machina (AEM)
## Copie des fichiers
### Si vous n’êtes pas familier avec un repository git
* Télécharger et décompresser l’archive en local
* Ajouter les infos dans le fichier `.env`, en fin de fichier, ajoutez les informations relatives au serveur de bases de données (adresse, identifiant, mot de passe, nom de la base de données) selon le modèle prévu à cet effet. La base de données par défaut est au format `PostGreSQL` : décommentez la ligne correspondant au type de base de données que vous désirez utiliser et remplissez les informations.
* Copiez l’ensemble des fichiers dans un répertoire nommé agoraexmachina

### Si vous êtes familier avec un repository git
* Utilisez `git clone` pour télécharger l'ensemble des fichiers nécessaires à  l'installation. 
## Installation
### Préambule
**AgoraExMachina** est développé à l'aide du Framework Symfony. Il est nécessaire :

* soit d'installer le gestionnaire de paquets `Composer` à votre serveur php/MySQL
* soit d'utiliser `composer.phar` pour permettre d'acquisition des paquets.

### Avec composer

* `composer install
* Editer le fichier `env.txt` pour indiquer l'adresse de la base de données et le serveur mail (lignes 19, 27 à 30 selon le serveur de base de données que vous utilisez en indiquant les informations relatives au serveur et 36 en indiquant les informations relatives à votre serveur mail). Enregistrez le avec le nom `.env.local`
* `php bin/console doctrine:database:create` (uniquement si elle n'a pas été créée auparavant).
* `php bin/console doctrine:migration:migrate`
### Avec composer.phar
* `php composer.phar install` dans le répertoire agoraexmachina*
* Editer le fichier `env.txt` pour indiquer l'adresse de la base de données et le serveur mail (lignes 19, 27 à 30 selon le serveur de base de données que vous utilisez en indiquant les informations relatives au serveur et 36 en indiquant les informations relatives à votre serveur mail). Enregistrez le avec le nom `.env.local` 
* `php bin/console doctrine:database:create` (uniquement si elle n'a pas été créée auparavant).
* `php bin/console doctrine:migration:migrate`
### Ubuntu
Dans un configuration générale en local sous Ubuntu, il peut ariver que des problèmes de droits se produisent. Si la méthode Composer décrite plus haut ne fonctionne pas, préférer les informations suivantes, sans tenir compte des avertissements :

* `sudo composer install`
* Editer le fichier `env.txt` pour indiquer l'adresse de la base de données et le serveur mail (lignes 19, 27 à 30 selon le serveur de base de données que vous utilisez en indiquant les informations relatives au serveur et 36 en indiquant les informations relatives à votre serveur mail). Enregistrez le avec le nom `.env.local`
* `sudo php bin/console doctrine:database:create`  (uniquement si elle n'a pas été créée auparavant).
* `php bin/console doctrine:migration:migrate`

### Droits d'utilisateur
Il sera également nécessaire (sous Mac/MAMP, Linux/LAMP ous dans un serveur distant) de modifier les droits d'utilisateurs récursivement, soit avec un logiciel sftp, soit en ligne de commande :

* `chmod 777 -R public/img/upload`
* `chmod 777 -R public/pdf/upload`

La modification des droits d'autres répertoires, notamment `/templates`, `/translations`, `public/css` ou `public/js` n'influe pas sur le fonctionnement de la plateforme et peut être utile pour faciliter le développement de l'aspect public du site par exemple.
### Procédure post-installation
* Dans un navigateur, se placer dans l’interface d’administration de AEM (http://mondomaine.com/agoraexmachina/public/login)
* S'enregistrer en admin (admin@mail.com / Password0@)
* Pour configurer le mailer, il faut l'adapter dans le fichier .env.local. Pour plus d'informations,  [Consultez l'aide de Symfony](https://symfony.com/doc/current/mailer.html).

Si les problèmes persistent, il faut changer le regex utilisé pour récupérer l'email dans la fonction sendEmailToUser dans l'entité User.

### Et ensuite
* Il est possible de modifier un certain nombre de variables dans le fichier `config/services.yaml`. parmi celles-ci :
  * le nom de la plateforme (affiché dans le footer)
  * Le langage, la lang et l'écriture (pour certaines balises, voir dans le fichier `templates/base.html.twig` pour plus de détails).
## Remarques
Pour tout bug relatif à l'installation, veuillez vous adresser aux auteurs  à l'adresse [aem@mobile-adenum.fr](mailto:aem@mobile-adenum.fr). Vous pouvez également vous adresser aux développeurs :

* Cyril Bazin : crlbazin@gmail.com
* Valentin Trouillet : trouillet.valentin@gmail.com
* Keyttlin Malicieux : keyttlin.m@hotmail.com
* Mehdi Larek : mehdi.larek@campus-eni.fr
* Baptiste Tielles : baptiste.tielles@outlook.fr.

Le projet **AgoraExMachina** est défendu par l'association **ADENÜM**.