<?php


namespace App\Form;


use App\Entity\Category;
use App\Entity\User;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CategoryType extends AbstractType
{
    /**
     * Une catégorie a un nom et une liste d'utilisateurs qu'on pout y ajouter.
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'name'
            ])
            ->add('description', CKEditorType::class, [
                'config' => [
                    'uiColor' => '#ffffff',
                    'toolbar' => 'full',
                ],
            ])
            ->add('users', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'expanded' => true,
                'label' => 'username'
            ])

            ->add('imageFile', VichImageType::class, [
                //'label' => 'image_file',
                'label' => ' ',
                'required' => false,
                'allow_delete' => true,
                'help'=>'img.max.size.2048',


            ]);
//        $builder->addEventListener( FormEvents::PRE_SUBMIT, function (FormEvent $event){
//            $form = $event->getForm();
//            $data= $form->get('users');
//
//           if (!isset($data['users']))
//            {$data['users']=[];}
//            $event->setData($data)
//        ;
//        }
//        );

         $builder   ->add('submit', SubmitType::class, [
                'label' => 'submit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class
        ]);
    }


}