<?php

namespace App\Form;

use App\Entity\Keyword;
use App\Entity\Workshop;
use App\Entity\Theme;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Symfony\Component\Validator\Constraints\File;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class WorkshopType extends AbstractType
{
    /**
     * Pour créer un atelier, doivent être renseignés :
     * - Le thème parent
     * - Le nom de l'atelier
     * - La description de l'atelier
     * - Une image (facultatif)
     * - Une date de début et une date de fin de discussion
     * - Une date de début et une date de fin de vote
     * - Des mots-clés (facultatif)
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
     //   $categories = $options['categories'];
        $builder
            ->add('theme', EntityType::class, [
                'label' => 'theme',
                'class' => Theme::class,
                'choice_label' => 'name',
                'multiple' => false,
              //  'choices' => $categories
            ])
            ->add('name', TextType::class,['label' => 'name'])
            ->add('description', CKEditorType::class, [
                'config' => [
                    'uiColor' => '#ffffff',
                    'toolbar' => 'full',
                ],
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'image_file',
                'required' => false,
                'allow_delete' => true,
                'help'=>'img.max.size.2048',
            ])
            ->add('dateBegin',DateType::class,['label' => 'date_begin'])
            ->add('dateEnd',DateType::class,['label' => 'date_end'])
            ->add('dateVoteBegin',DateType::class,['label' => 'date_vote_begin'])
            ->add('dateVoteEnd',DateType::class,['label' => 'date_vote_end'])

            /*->add('rightsSeeWorkshop', ChoiceType::class, [
                'label'=>"rights.workshops.see",
                'choices' => ['Everyone' => 'everyone']
            ])*/

            ->add('rightsVoteProposals', ChoiceType::class, [
                'label'=>'rights.proposals.vote',
                'choices' => ['Everyone' => 'everyone']])

            /*->add('rightsWriteProposals', ChoiceType::class, [
                'label'=>'rights.proposals.write',
                'choices' => ['Everyone' => 'everyone']])*/

            ->add('writeProposals',CheckboxType::class,[
                'label'=>'rights.proposals',
                'mapped' => false,
                'required' => false
            ])

            ->add('quorumRequired', IntegerType::class, [
                'label'=>'quorum.required',
                'help' => 'percentage.required',
                'attr' =>   ['min' => 0,
                            'max' => 100]
            ])
            ->add('rightsDelegation',CheckboxType::class,['label'=>'rights.delegations'])

            ->add('anonymousVote', CheckboxType::class, [
                'label' => 'anonymous.votes',
                'required' => false
            ])

            ->add('keytext', TextType::class, [
                'help' => 'keyword.help',
                'label' => 'keyword',
                'required' => false,
                'attr'=>['autocomplete' => 'off',],
            ])
            ->add('Submit', SubmitType::class, ['label'=>'submit']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Workshop::class,
            'validation_groups' => ['Default','dateBegin_edit','new_add']
        ]);

            if ('dateEnd'>'today' )

             $resolver->setDefaults([
                'data_class' => Workshop::class,
                'validation_groups' => ['Default','new_add']
            ]);
     //   $resolver->setRequired(['categories']);
        }

}