<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Cgu;
use Doctrine\DBAL\Types\StringType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class CguType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'name'
            ])
            ->add('category', EntityType::class, [
                'label' => 'category',
                'class' => Category::class,
                'choice_label' => 'name',
                'disabled' => true
            ])
            ->add('pdfFilename', FileType::class,[
                'label' => 'pdf',
                'mapped' => 'false',
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                            'text/plain'

                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF or MD document',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cgu::class,
        ]);
    }
}
