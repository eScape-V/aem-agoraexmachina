<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\UserAddFormType;
use App\Form\UserCsvType;
use App\Form\UserEditByUserType;
use App\Repository\CategoryRepository;
use App\Repository\ForumRepository;
use App\Repository\ProposalRepository;
use App\Repository\ThemeRepository;
use App\Repository\UserRepository;
use App\Repository\WebsiteRepository;
use App\Repository\WorkshopRepository;
use App\Service\Service;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\UserEditFormType;
use App\Security\LoginFormAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class SecurityController Cette classe gère les utilisateurs. Leurs connexions, déconnexions, inscriptions,
 * modifications etc..
 * @package App\Controller
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response Cette fonction gère la connexion de l'utilisateur et les erreurs éventuelles
     */
    public function login(AuthenticationUtils $authenticationUtils, Service $service, Request $request): Response
    {
        $service->dateVerification();


        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
       
        return $this->render('security/login.html.twig', ['error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     * Cette fonction s'occupe de la déconnexion de l'utilisateur
     */
    public function logout()
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
	
	/**
	 * @Route("/admin/user", name="user_admin")
     * @return Response Cette fonction affiche une liste des utilisateurs , elle est destinée aux administrateurs
	 */
	public function admin(UserRepository $userRepository, Request $request): Response
    {

        //Nombre de résultats par page
        $limit = 20;

        //la page qu'on veut afficher
        $page = (int)$request->query->get("page", 1);

        //les utilisateurs à afficher sur cette page
        $users = $userRepository->findAllWithPagination($page, $limit);

        //le nombre total d'utilisateur
        $total = $userRepository->getTotalUsers();

		#Envoi des utilisateurs à l'affichage
	    return $this->render('security/admin.html.twig',
				[
					'users' => $users,
                    'total' => $total,
                    'limit' => $limit,
                    'page' => $page
				]
				);		
	}

    /**
     * @Route("/admin/user/add", name="user_add")
     * @param UserPasswordEncoderInterface $passwordEncoder Encode le mot de passe de l'utilisateur
     * @param Request $request Requête gérant le formulaire d'inscription
     * @return Response Cette fonction permet d'ajouter un utilisateur à la BDD. Elle est destinée à un administrateur
     */
	public function addUser(UserPasswordEncoderInterface $passwordEncoder,Request $request): Response
    {
        # Crée un utilisateur et génère le formulaire nécessaire
        $user = new User();
        $form = $this->createForm(UserAddFormType::class,$user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            #Encode le mot de passe donné dans le formulaire
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setIsAllowedEmails(false);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            unset($user);
            unset($form);
            $user = new User();
            $form = $this->createForm(UserAddFormType::class,$user);


            $this->addFlash("success", "add.success");
            if (isset($_POST['continue'])){
                return $this->render('security/add.html.twig', [
                    'userForm' => $form->createView(),
                ]);
            }else{
                return $this->redirectToRoute('user_admin');
            }
        }


        return $this->render('security/add.html.twig', [
            'userForm' => $form->createView()]);

        //TODO: regrouper les formulaires et les twig edit et add


    }

    /**
     * @Route("/admin/user/delete/{id}", name="user_delete")
     * @param int $id Utilisateur à supprimer
     * @return RedirectResponse Fonction qui supprime un utilisateur et redirige vers l'index des utilisateurs.
     * Destiné à un administrateur
     */
	public function delete(int $id, UserRepository $userRepository, EntityManagerInterface $entityManager): RedirectResponse
    {

		$user = $userRepository->find($id);
        $this->deleteUser($user, $entityManager);


		# Affichage d'un flash indiquant le succès et redirection
		$this->addFlash("success", "delete.success");
		return $this->redirectToRoute('user_admin');

	}

    /**
     * @Route("/user/accountDelete", name="user_account_delete")
     * Destiné à un utilisateur non-admin. 'Supprime' le compte sur lequel on est connecté.
     */
    public function accountDelete(EntityManagerInterface $entityManager): RedirectResponse
    {
        $user = $this->getUser();
        $this->deleteUser($user, $entityManager);

        $currentId = $this->getUser()->getId();
        if ($currentId == $user->getId()){
            $session = $this->get('session');
            $session = new Session();
            $session->invalidate();
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/admin/user/edit/{id}", name="user_admin_edit")
     * @param Request $request Gère le formulaire
     * @param int $id Utilisateur à modifier
     * @return Response Fonction qui permet de modifier un utilisateur. Destiné à un administrateur
     */
	public function edit(Request $request, int $id): Response
    {
        #On récupère l'utilisateur
		$user = $this->getDoctrine()
				->getRepository(User::class)
				->find($id);

		$form = $this->createForm(UserEditFormType::class, $user);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            # Affichage d'un flash indiquant le succès et redirection
			$this->addFlash("success", "edit.success");
		}
		# Création de la page et du formulaire
        return $this->render('security/edit.html.twig', [
            'userForm' => $form->createView(),
            'user' => $user
        ]);
	}

    /**
     * @Route ("/user", name="user_edit_by_user")
     * @param Request $request Gère le formulaire
     * @param UserPasswordEncoderInterface $passwordEncoder Permet d'encoder le mot de passe
     * @param ForumRepository $forumRepository Répertoire des forums 
     * @param ProposalRepository $proposalRepository Répertoire des propositions
     * @param WorkshopRepository $workshopRepository Répertoire des ateliers
     * @return Response Cette fonction affiche une page multi-fonction avec nagivation par onglet. On peut y voir son
     * profil, qu'on peut modifier, ainsi que ses forums ouverts, ses propositions crées et ses ateliers existant 
     * dans les thèmes auxquels on est souscrit .
     * @throws TransportExceptionInterface
     */
	public function editByUser(Request $request, UserPasswordEncoderInterface
    $passwordEncoder, ForumRepository $forumRepository, ProposalRepository $proposalRepository, WorkshopRepository
    $workshopRepository, UserRepository $userRepository, ThemeRepository $themeRepository): Response
    {
        #On récupère l'utilisateur
        $user = $this->getUser();
        #On crée le formulaire pour permettre de modifier son profil
        $form = $this->createForm(UserEditByUserType::class,$user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            #Si le mot de passe est inclus dans la modification du profil, on l'encode
            if ($user->getPlainPassword() !== null) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
            }
            #Sauvegarde du nouveau profil
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash("success", "edit.success");
            //return $this->redirectToRoute('homepage');

            //on reste sur la meme page pour le traitement des photos
            return $this->redirectToRoute('user_edit_by_user');
        }


        #On récupère les forums, propositions et ateliers nécessaires pour les envoyer à la template
        /*$forums = $forumRepository->findBy(['user'=>$user]);
        $proposals = $proposalRepository->findBy(['user'=>$user]);
        $workshops = $workshopRepository->findBy(['user'=>$user]);*/

        //Meme chose qu'au dessus, mais avec des jointures, divise le nombre de requetes par deux
        $forums = $forumRepository->findForumsByUser($user);
        $proposals = $proposalRepository->findProposalByUser($user);
        $workshops = $workshopRepository->findWorkshopsByUser($user);
        $themes = $themeRepository->findThemesByUser($user->getId());
        $categories = $user->getCategories();




        return $this->render('security/edit-by-user.html.twig', [
            'userForm' => $form->createView(),
            'forums' => $forums,
            'proposals' => $proposals,
            'workshops' => $workshops,
            'categories' => $categories
        ]);

    }

    /**
     * @Route("/register", name="app_register")
     * @param MailerInterface $mailer Permet d'envoyer un email
     * @param Request $request Gère le formulaire
     * @param UserPasswordEncoderInterface $passwordEncoder Encode le mot de passe
     * @param GuardAuthenticatorHandler $guardHandler S'occupe de l'enregistrement
     * @param LoginFormAuthenticator $authenticator S'occupe de la connexion
     * @return Response Une fonction permettant à un utilisateur de s'enregistrer
     * @throws TransportExceptionInterface
     */
	public function register(MailerInterface $mailer,Request $request, UserPasswordEncoderInterface $passwordEncoder,
                              GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, WebsiteRepository $websiteRepository): Response
    {
        # On crée l'utilisateur et on crée le formulaire
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
#Si on enregistre un admin, il faut lui ajouter toutes les catégories.
        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setIsAllowedEmails(false);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $messageEnregistrement = $websiteRepository->find(1)->getMailRegistration();
            $mailAdmin = $websiteRepository->find(1)->getEmail();
            $nomSite = $websiteRepository->find(1)->getName();

            #Email de confirmation
            $email = (new Email());

            $email
                #->from('accounts@agora.com')
                /*->from(new Address('info@mobile-adenum.fr', 'Agora Ex Machina'))*/
                ->from(new Address($mailAdmin, $nomSite))
                ->to($user->getEmail())
                ->subject("Confirmation de l'inscription")
                #->htmlTemplate('email/report.html.twig')
                #give a link with a random password. Link will be something like public/setPw/userid
                ->text("Bonjour ".$user->getUsername()."\r\n".$messageEnregistrement);


            //J'ai sortis l'envois de mail de la condition, puisque le premier mail est forcement recu
            $mailer->send($email);

            # Retour à l'accueil en étant connecté
            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('security/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/editCategory/{id}", name="user_edit_category")
     * @param Request $request Gère la modification
     * @param int $id Identifiant de l'utilisateur
     * @return Response Fonction permettant de modifier les catégories de l'utilisateur
     */
    public function userEditCategories(Request $request,int $id): Response
    {
        # On récupère l'utilisateur, et toutes les catégories existantes, pour les envoyer à la template
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('security/edit-category.html.twig', [
            'categories' => $categories,
            'user' => $user
        ]);
    }

    /**
     * @Route("/admin/user/addCategory/{id}/{categoryId}", name="user_add_category")
     * @param int $id Identifiant de l'utilisateur
     * @param int $categoryId Identifiant de la catégorie
     * @return Response Fonction qui ajoute une catégorie à un utilisateur
     */
    public function userAddCategory(int $id, int $categoryId): Response
    {
        # On récupère l'utilisateur et la catégorie demandée et on l'ajoute
        $category = $this->getDoctrine()->getRepository(Category::class)->find($categoryId);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        $user->addCategory($category);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('security/edit-category.html.twig', [
            'categories'=> $this->getDoctrine()->getRepository(Category::class)->findAll(),
            'user' => $user
        ]);

    }

    /**
     * @Route("/admin/user/removeCategory/{id}/{categoryId}", name="user_remove_category")
     * @param int $id Identifiant de l'utilisateur
     * @param int $categoryId Identifiant de la catégorie
     * @return Response Fonction qui enlève une catégorie à un utilisateur
     */
    public function userRemoveCategory(int $id, int $categoryId): Response
    {
        #TODO: Empecher l'acces a une page erreur en changeant l'URL ?
        # On récupère l'utilisateur et la catégorie demandée et on l'enlève
        $category = $this->getDoctrine()->getRepository(Category::class)->find($categoryId);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        $user->removeCategory($category);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('security/edit-category.html.twig', [
            'categories'=> $this->getDoctrine()->getRepository(Category::class)->findAll(),
            'user' => $user
        ])
            ;

    }




    /**
     * @Route("/admin/user/csv", name="user_csv")
     */
    public function addUserCsv(Request $request, EntityManagerInterface $entityManager,
                               UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator,
                                UserRepository $userRepository, SluggerInterface $slugger){

        //permet d'effacer le dernier fichier prévisualiser en cas d'annulation
        if (isset($_GET['filename'])){

            if(file_exists($this->getParameter('pdf_directory').'/'.$_GET['filename'])){
                unlink($this->getParameter('pdf_directory').'/'.$_GET['filename']);
            }
        }

       $form = $this->createForm(UserCsvType::class);
       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()){

           $filePreview =
               "<table style='width:100%;border: 1px solid black; border-collapse: collapse'>
               <thead>
                <tr><td style='border: 1px solid black; border-collapse: collapse; padding:0.2rem;'>Username</td>
               <td style='border: 1px solid black; border-collapse: collapse;padding:0.2rem;'>First Name</td>
               <td style='border: 1px solid black; border-collapse: collapse;padding:0.2rem;'>Last Name</td>
               <td style='border: 1px solid black; border-collapse: collapse;padding:0.2rem;'>Email</td>
               <td style='border: 1px solid black; border-collapse: collapse;padding:0.2rem;'>Password</td></tr><thead><tbody>";

        $file = $form->get('csvFile')->getData();

        $handle = fopen($file->getPathname(), 'r');
        if ($handle !== false){

             //permet de sauter la premiere ligne du tableau
             fgetcsv($handle);

            while (($column = fgetcsv($handle, 1000, ';')) !== false){


                $user = new User();
                $user->setUsername($column[0]);
                //on quitte le traitement si on trouve une ligne vide pour éviter les erreurs
                if ($user->getUsername() == null or $user->getUsername() == ' '){
                    continue;
                }
                $user->setFirstName($column[1]);
                $user->setLastName($column[2]);
                $user->setEmail($column[3]);

                if ($column[4] == null || $column[4] == ' '){
                    $user->setPlainPassword('Password0@');
                }else{
                    $user->setPlainPassword($column[4]);
                }


                $user->setIsAllowedEmails(false);

                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $user->getPlainPassword()
                    )
                );

                if (isset($_POST['preview'])){
                    $filePreview .= "
                   <tr style='border: 1px solid black; border-collapse: collapse'><td>".$user->getUsername()."</td>
                   <td>".$user->getFirstName()."</td>
                   <td>".$user->getLastName()."</td>
                   <td>".$user->getEmail()."</td>
                   <td>".$user->getPlainPassword()."</td></tr>";
                }

                //Vérification si les pseudos et les mails sont libres
                if ($userRepository->findOneBy(['username' => $user->getUsername()])){
                    $this->addFlash('error', $translator->trans('username.taken').$user->getUsername());
                    return $this->redirectToRoute('user_csv');
                }
                if ($userRepository->findOneBy(['email' => $user->getEmail()])){
                    $this->addFlash('error', $translator->trans('email.taken').$user->getEmail());
                    return $this->redirectToRoute('user_csv');
                }


                $entityManager->persist($user);
            }
            fclose($handle);

            if (isset($_POST['preview'])){

                //Je sauvegarde le fichier en local pour le récupérer sur la page de prévisualisation
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = 'preview'.$safeFilename.'-'.uniqid().'.'.$file->guessExtension();
                try{
                    $file->move(
                        $this->getParameter('pdf_directory'), $newFilename
                    );
                }catch (FileException $e){

                }

                $filePreview .= "</tbody></table>";

                return $this->render('security/preview-csv.html.twig', [
                    'filePreview'=> $filePreview,
                    'filename' => $newFilename
                ]);

            }

            $entityManager->flush();
            $this->addFlash('success', $translator->trans('file.imported') );
            return $this->redirectToRoute('user_admin');
         }
       }

        return $this->render('security/add-user-csv.html.twig', [
            'form' => $form->createView(),
        ]);

    }


    /**
     * @Route("/admin/user/csvfile/{filename}", name="user_csvfile")
     */
    public function addCsvWithFile( $filename, UserPasswordEncoderInterface $passwordEncoder,
                                    UserRepository $userRepository, TranslatorInterface $translator,
                                    EntityManagerInterface $entityManager, SluggerInterface $slugger){

       $file = new File($this->getParameter('pdf_directory').'/'.$filename);

        $handle = fopen($file->getPathname(), 'r');
        if ($handle !== false){
            fgetcsv($handle);
            while (($column = fgetcsv($handle, 1000, ';')) !== false){
                $user = new User();
                $user->setUsername($column[0]);

                if ($user->getUsername() == null or $user->getUsername() == ' '){
                    continue;
                }
                $user->setFirstName($column[1]);
                $user->setLastName($column[2]);
                $user->setEmail($column[3]);

                if ($column[4] == null || $column[4] == ' '){
                    $user->setPlainPassword('Password0@');
                }else{
                    $user->setPlainPassword($column[4]);
                }

                $user->setIsAllowedEmails(false);

                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $user->getPlainPassword()
                    )
                );
                //Vérification si les pseudos et les mails sont libres
                if ($userRepository->findOneBy(['username' => $user->getUsername()])){
                    $this->addFlash('error', $translator->trans('username.taken').$user->getUsername());
                    return $this->redirectToRoute('user_csv');
                }
                if ($userRepository->findOneBy(['email' => $user->getEmail()])){
                    $this->addFlash('error', $translator->trans('email.taken').$user->getEmail());
                    return $this->redirectToRoute('user_csv');
                }
                $entityManager->persist($user);
            }
            fclose($handle);
            $entityManager->flush();
        }
        unlink($this->getParameter('pdf_directory').'/'.$filename);
        $this->addFlash('success', $translator->trans('file.imported') );
        return $this->redirectToRoute('user_admin');
    }


    /**
     * @Route("/admin/export/csv", name="export_csv")
     */
    public function exportCsv(){
        $file = "Username; First Name; Last Name; Email; Password\n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";
        $file .= " ; ; ; ; \n";

        $response = new Response($file);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename=user.csv');
        return $response;
    }





    #Fonction pour effacer un utilisateur et le remplacer par un utilisateur par defaut, je la mets à part pour l'utiliser dans deux méthodes différentes
    #(celle de l'admin, et celle de l'user)

    public function deleteUser($user, EntityManagerInterface $entityManager){

        #Pour garder son vote et son pseudo, on ne peut pas l'effacer réellement
        #Le password n'est pas haché, impossible de se connecter avec
        $user->setPassword('effacé');
        $user->setEmail($user->getUsername().'.deleted');
        $user->setIsAllowedEmails(false);

        if ($user->getImage()){
            unlink('img/upload/users/'.$user->getImage());
            $user->setImage(null);
        }

        $user->setFirstName(' ');
        $user->setLastName(' ');

        $entityManager->persist($user);
        $entityManager->flush();


        /*#On récupère les ateliers dont il est l'auteur
        $workshops = $workshopRepository->findBy(['user' => $user]);

        #On récupere aussi ses propositions
        $proposals = $proposalRepository->findBy(['user' => $user]);

        #Et ses messages
        $forums = $forumRepository->findBy(['user' => $user]);


        #Evite les erreurs si on supprime le compte sur lequel on est connecté
        $currentId = $this->getUser()->getId();
        if ($currentId == $user->getId()){
            $session = $this->get('session');
            $session = new Session();
            $session->invalidate();
        }

        #On récupère l'utilisateur anonyme, ou on le crée s'il n'existe pas encore
        $anon = $userRepository->findAnonUser();
        if (!$anon){
            $anon = $service->createAnonUser();
        }


        #On remplace l'utilisateur à effacer avec l'anonyme
        foreach ($workshops as $workshop){
            $workshop->setUser($anon);
            $entityManager->persist($workshop);
        }
        foreach ($proposals as $proposal){
            $proposal->setUser($anon);
            $entityManager->persist($proposal);
        }
        foreach ($forums as $forum){
            $forum->setUser($anon);
            $entityManager->persist($forum);
        }

        $entityManager->remove($user);
        $entityManager->flush();*/
    }

    
}
