<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use App\Repository\ProposalRepository;
use App\Repository\RequestRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class NotificationController Cette classe s'occupe de l'affichage des notifications
 * @package App\Controller
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/notification", name="notification")
     * @return Response Affichage une page contenant toutes les notifications de l'utilisateurs
     */
    public function index(RequestRepository $requestRepository): Response
    {
        //Je vais chercher les requetes pour les mettre à disposition de Doctrine,
        //et eviter qu'il les recupere une par une.
        $requests = $requestRepository->findRequest();

        #Récupère toutes les notifications de l'utilisateur
        $notifications = $this->getDoctrine()->getRepository(Notification::class)->findByUserId($this->getUser()
            ->getId());
        $entityManager = $this->getDoctrine()->getManager();
        #Pour toute notification non lue, on les change en lue en affichant la page
        foreach ($notifications as $notification)
        {
            if (!$notification->getIsRead())
            {
                $notification->setIsRead(true);
                $entityManager->persist($notification);
            }
        }
        $entityManager->flush();

        return $this->render('notification/index.html.twig', [
            'notifications'=>$notifications,
        ]);
    }


    /**
     * @Route("/notification/delete/{id}", name="notification_delete")
     */
    public function delete(int $id,
                    EntityManagerInterface $entityManager, NotificationRepository $notificationRepository
                        , TranslatorInterface $translator){

        $notification = $notificationRepository->find($id);
        $entityManager->remove($notification);
        $entityManager->flush();

        $this->addFlash('success', $translator->trans('notification.delete'));

        return $this->redirectToRoute('notification');


    }

}
