<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\Report;
use App\Repository\ForumRepository;
use App\Repository\ProposalRepository;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use App\Repository\WorkshopRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ReportController extends AbstractController
{
    /**
     * Cette fonction retourne un json a un appel ajax. Ce JSON contient tous les signalements.
     * @Route("/report/fetch", name="report_ajax")
     */
    public function ajxFetchReports(\Symfony\Component\HttpFoundation\Request $request,ReportRepository $reportRepository): JsonResponse
    {
        #Si la requete est une requete AJAX, on renvoi en json les signalements recents
        if($request->isXmlHttpRequest())
        {
            $reports = $reportRepository->findAll();
            $jsonData = array();
            $idx = 0;
            foreach($reports as $report) {
                # On envoie les donnes de chaque forums dans un json
                $temp = array(
                   'id' => $report->getForum()->getId(),
                   'about' => $report->getUser()->getUsername() ,
                   'date' => $report->getDate(),
                   'forum-name' => $report->getForum()->getName(),
                   'forum' => $report->getForum()->getDescription(),
                );
                $jsonData[$idx++] = $temp;
            }

            return new JsonResponse($jsonData);
        }
        return new JsonResponse();
    }


    /**
     * @Route("/report/forum/{slug}/{idForum}", name="report_forum")
     */
    public function reportForum($idForum, $slug, ForumRepository $forumRepository,
                                EntityManagerInterface $entityManager, UserRepository $userRepository,
                                TranslatorInterface $translator): Response{

        $message = $forumRepository->find($idForum);
        $user = $message->getUser();
        $reporter = $this->getUser();

        $report = new Report();
        $report->setDate(new \DateTime());
        $report->setUser($user);
        $report->setForum($message);
        $report->setReporter($reporter);
        $report->setIsDone(false);

        $admins = $userRepository->findAdmins();
        $adminsRestreint = $userRepository->findAdminsRestreint();
        foreach ($admins as $admin){
            /*$notification = $admin->prepareNotification($reporter->getUsername().' a signalé '.$user->getUsername().' pour le message: '.$message->getName());*/
            $notification = $admin->prepareNotification(
                '<i style="color: blue">'.$reporter->getUsername(). '</i> a signalé <i style="color: red"> '.$user->getUsername().'</i> pour le message: <br>'.$message->getName().'<br>'.$message->getDescription()
            );
            $notification->setReport($report);
            $entityManager->persist($notification);
        }
        foreach ($adminsRestreint as $adminR){
           foreach ($adminR->getCategories() as $cat){
               if ($message->getProposal()->getWorkshop()->getTheme()->getCategory() == $cat){
                   $notification = $adminR->prepareNotification(
                       '<i style="color: blue">'.$reporter->getUsername(). '</i> a signalé <i style="color: red"> '.$user->getUsername().'</i> pour le message: <br>'.$message->getName().'<br>'.$message->getDescription()
                   );
                   $notification->setReport($report);
                   $entityManager->persist($notification);
               }
           }
        }

        $entityManager->flush();
        $this->addFlash('success', $translator->trans('report.forum'));

        return $this->redirectToRoute('workshop_show', [
                'slug' => $slug,
                'workshop' => $message->getProposal()->getWorkshop()->getId(),
            ]
        );
    }


    /**
     * @Route("/report/proposal/{slug}/{idProposal}", name="report_proposal")
     */
    public function reportProposal($slug, $idProposal, ProposalRepository $proposalRepository,
                                   EntityManagerInterface $entityManager, UserRepository $userRepository,
                                    TranslatorInterface $translator){
        $proposal = $proposalRepository->find($idProposal);
        $author = $proposal->getUser();
        $reporter = $this->getUser();

        $report = new Report();
        $report->setDate(new \DateTime());
        $report->setUser($author);
        $report->setProposal($proposal);
        $report->setReporter($reporter);
        $report->setIsDone(false);

        $admins = $userRepository->findAdmins();
        $adminsRestreint = $userRepository->findAdminsRestreint();

        foreach ($admins as $admin){
           $notification = $admin->prepareNotification(
                '<i style="color: blue">'.$reporter->getUsername(). '</i> a signalé la proposition de <i style="color: red">'.$author->getUsername().'</i><br>'.$proposal->getName().'<br>'.$proposal->getDescription()
           );
           $notification->setReport($report);
           $entityManager->persist($notification);
        }
        foreach ($adminsRestreint as $adminR){
            foreach ($adminR->getCategories() as $cat){
                if ($proposal->getWorkshop()->getTheme()->getCategory() == $cat){
                    $notification = $adminR->prepareNotification(
                        '<i style="color: blue">'.$reporter->getUsername(). '</i> a signalé la proposition de <i style="color: red">'.$author->getUsername().'</i><br>'.$proposal->getName().'<br>'.$proposal->getDescription()
                    );
                    $notification->setReport($report);
                    $entityManager->persist($notification);
                }
            }
        }

        $entityManager->flush();
        $this->addFlash('success', $translator->trans('report.proposal'));

        return $this->redirectToRoute('workshop_show', [
                'slug' => $slug,
                'workshop' => $proposal->getWorkshop()->getId(),
            ]
        );
    }

    /**
     * @Route("/report/process/{idReport}/{decision}", name="report_process")
     */
    public function processReport($decision, $idReport, ReportRepository $reportRepository,
                                    EntityManagerInterface $entityManager, TranslatorInterface $translator): Response{


        $report = $reportRepository->find($idReport);
        $message = $report->getForum();
        $reports = $message->getReports();

        #Message non hors charte, on passe les signalements sur traité
        foreach ($reports as $reprt) {
            $reprt->setIsDone(true);
            $entityManager->persist($reprt);
        }


        #Message hors charte, on l'efface
        if ($decision == 'true' || $decision == 'ban') {

            #Si l'admin décide de bannir l'auteur
            if ($decision == 'ban'){
                $author = $message->getUser();
                $author->addBannedFrom($message->getProposal()->getWorkshop());

                $notificationBan = $author->prepareNotification('<p style="color: red">'.$translator->trans('ban.from').$message->getProposal()->getWorkshop()->getName().'</p>');
                $entityManager->persist($notificationBan);
                $entityManager->persist($author);
            }


            #On récupère toutes les notifications liées à ce message et tous les signalements
            foreach ($reports as $reprt) {
                foreach ($reprt->getNotifications() as $notif) {
                    $reprt->removeNotification($notif);
                    $entityManager->persist($notif);
                    $entityManager->remove($reprt);
                }
            }

            #Si le message est une réponse, on l'enlève de son parent
            if ($message->getParentForum() != null) {
                $message->getParentForum()->removeForum($message);
            }

            #On efface les réponses du message en cause
            foreach ($message->getForums() as $answer) {
                $message->removeForum($answer);
                $entityManager->remove($answer);
            }

            $entityManager->remove($message);
        }

        $entityManager->flush();
        $this->addFlash('success', $translator->trans('report.handled'));

        return $this->redirectToRoute('notification');
    }


    /**
     * @Route("/report/process/proposal/{idReport}/{decision}", name="report_process_proposal")
     */
    public function processReportProposal($idReport, $decision, ReportRepository $reportRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator){
        $report = $reportRepository->find($idReport);
        $proposal = $report->getProposal();

        $reports = $proposal->getReports();

        foreach ($reports as $reprt) {
            $reprt->setIsDone(true);
            $entityManager->persist($reprt);
        }

        if ($decision == 'true' || $decision == 'ban'){
            if ($decision == 'ban'){
                $author = $proposal->getUser();
                $author->addBannedFrom($proposal->getWorkshop());

                $notificationBan = $author->prepareNotification('<p style="color: red">'.$translator->trans('ban.from').$proposal->getWorkshop()->getName().'</p>');
                $entityManager->persist($notificationBan);
                $entityManager->persist($author);
            }
            #On récupère toutes les notifications liées à cette proposition et tous les signalements
            foreach ($reports as $reprt) {
                foreach ($reprt->getNotifications() as $notif) {
                    $reprt->removeNotification($notif);
                    $entityManager->persist($notif);
                    $entityManager->remove($reprt);
                }
            }


            $messages = $proposal->getForums();
            foreach ($messages as $message){
                if ($message->getReports()){
                    foreach ($message->getReports() as $r){
                        $message->removeReport($r);
                        $entityManager->persist($message);
                    }
                }
            }
            $entityManager->remove($proposal);
        }

        $entityManager->flush();
        $this->addFlash('success', $translator->trans('report.handled'));

        return $this->redirectToRoute('notification');
    }

    /**
     * @Route("/report/unban/{idWorkshop}/{idUser}", name="report_unban")
     */
    public function unban($idWorkshop, $idUser, WorkshopRepository $workshopRepository,
                          UserRepository $userRepository, EntityManagerInterface $entityManager){

        $workshop = $workshopRepository->find($idWorkshop);
        $user = $userRepository->find($idUser);

        $workshop->removeBannedUser($user);
        $entityManager->persist($workshop);
        $entityManager->flush();

        return $this->redirectToRoute('user_admin_edit', [
            'id' => $user->getId()
        ]);

    }




}
