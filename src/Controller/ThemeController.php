<?php

namespace App\Controller;

use App\Entity\Theme;
use App\Entity\User;
use App\Form\ThemeType;
use App\Repository\ThemeRepository;
use App\Repository\WorkshopRepository;
use App\Service\Service;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ThemeController Cette classe gère la gestion des Thèmes et leur affichage
 * @package App\Controller
 */
class ThemeController extends AbstractController
{

    /**
     * @Route("/admin/theme", name="theme_admin", methods={"GET"})
     * @return Response Une fonction qui affiche les thèmes pour l'administrateur
     */
    public function admin(MailerInterface $mailer, ThemeRepository $themeRepository, Request $request): Response
    {
       #$user=$this->getUser();
       #$user->sendEmailToUser($mailer,$this->getParameter('app.smtp_email'),'TEST','Bonjour!');

        //Nombre de résultats par page
        $limit = 20;

        //la page qu'on veut afficher
        $page = (int)$request->query->get("page", 1);

        //les themes à afficher sur cette page
        $themes = $themeRepository->findAllWithPagination($page, $limit);

        //le nombre total de themes
        $total = $themeRepository->getTotalThemes();


        return $this->render('theme/admin.html.twig', [
            'themes' => $themes,
            'total' => $total,
            'limit' => $limit,
            'page' => $page
        ]);
    }

    /**
     * @Route("/admin/theme/add", name="theme_add", methods={"GET", "POST"})
     * @param Request $request Gère le formulaire
     * @return Response Une fonction qui permet d'ajouter un thème à la BDD
     */
    public function add(Request $request): Response
    {
        #On crée un thème et le formulaire nécessaire
        $theme = new Theme();
        $user = $this->getUser();
        $form = $this->createForm(ThemeType::class, $theme,
        [
            'user' => $user,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($theme->getVoteType() !== 'weighted' and $theme->getVoteType() !== 'weighted-delegation'){
                $theme->setPoints(null);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($theme);
            $entityManager->flush();

            # Ajout d'un message flash indiquant le succès et redirection vers la page pour modifier le thème en
            # question
            $this->addFlash("success", "add.success");
            return $this->redirectToRoute('theme_edit', ["theme" => $theme->getId()]);
        }

        return $this->render('theme/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/theme/edit/{theme}", defaults={"theme"=1}, name="theme_edit", methods={"GET", "POST"})
     * @param Request $request Gère le formulaire
     * @param Theme $theme Thème à modifier
     * @return Response Fonction qui permet de modifier un thème
     */
    public function edit(Request $request, Theme $theme): Response
    {
        # On verifie que l'admin restreint est enregistré a cette catégorie ou que c'est un admin
        # Sinon on le redirige avec un message flash le prévenant qu'il n'a pas les droits
        $admin = $this->getUser();
        $users = $theme->getCategory()->getUsers();
        $user = $this->getUser();
        if (
            !(
                in_array('ROLE_ADMIN_RESTRICTED', $admin->getRoles())
                &&
                $users->contains($admin)
            ||
            in_array('ROLE_ADMIN', $admin->getRoles()))
        ) {

            $this->addFlash("warning", "edit.authorization");
            return $this->redirectToRoute('theme_edit',["theme" => $theme->getId()]);
        }

        #Création du formulaire à partir du thème et gestion via la requête
        $form = $this->createForm(ThemeType::class, $theme,
            [
                'user' => $user,
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($theme->getVoteType() !== 'weighted' and $theme->getVoteType() !== 'weighted-delegation'){
                $theme->setPoints(null);
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "edit.success");
            //return $this->redirectToRoute('theme_admin');

            //On reste sur la meme page, pour la gestion des photos
            return $this->redirectToRoute('theme_edit', [
                'theme' => $theme->getId()
            ]);
        }

        return $this->render('theme/edit.html.twig', [
            'form' => $form->createView(),
            'theme' => $theme
        ]);
    }

    /**
     * @Route("/admin/theme/delete/{theme}", name="theme_delete", methods={"GET"})
     * @param Request $request
     * @param Theme $theme Le thème à supprimer
     * @return Response Fonction qui supprime un thème
     */
    public function delete(Request $request, Theme $theme): Response
    {
        # On verifie que l'admin restreint est enregistré a cette catégorie
        $admin = $this->getUser();
        $users = $theme->getCategory()->getUsers();
        if (!(
            in_array('ROLE_ADMIN', $admin->getRoles())
            ||
            in_array('ROLE_ADMIN_RESTRICTED', $admin->getRoles())

            &&
            $users->contains($admin)
        ))
        {
            #Redirection si non-droit
            return $this->redirectToRoute('theme_admin');
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($theme);
        $entityManager->flush();
        # Suppression du thème, redirection vers la page d'index des thèmes dans l'administration et affichage d'un
        # message indiquant le succès de l'opération
        $this->addFlash("success", "delete.success");
        return $this->redirectToRoute('theme_admin');
    }


    /**
     * @Route("/admin/theme/copy/{themeId}", name="theme_copy")
     */
    public function copy($themeId, EntityManagerInterface $entityManager,
                         ThemeRepository $themeRepository, WorkshopRepository $workshopRepository,
                            Service $service, TranslatorInterface $translator): Response{
        $theme = $themeRepository->find($themeId);
        $i = $themeRepository->getCopyCount($theme->getName());

        $themeCopy = new Theme();
        $themeCopy->setCategory($theme->getCategory());
        $themeCopy->setName($theme->getName().' Copie '.$i);
        $themeCopy->setDescription($theme->getDescription());
        $themeCopy->setIsPublic($theme->getIsPublic());
        $themeCopy->setVoteType($theme->getVoteType());
        if ($theme->getPoints() != null){
            $themeCopy->setPoints($theme->getPoints());
        }
        if ($theme->getDelegationDeepness() != null){
            $themeCopy->setDelegationDeepness($theme->getDelegationDeepness());
        }
        if ($theme->getImage() != null){
            $themeCopy->setImage($theme->getImage());
        }

        $entityManager->persist($themeCopy);


        $workshops = $workshopRepository->findBy(['theme' => $theme]);
        foreach ($workshops as $workshop){
            $newWorkshop = $service->copyWorkshop($workshop, $this->getUser());
            $newWorkshop->setTheme($themeCopy);
            $entityManager->persist($newWorkshop);
        }

        $entityManager->flush();

        $this->addFlash('success', $translator->trans('copy.success'));

        return $this->redirectToRoute('theme_admin');

    }

}