<?php

namespace App\Controller;

use App\Entity\Cgu;
use App\Form\CguType;
use App\Repository\CategoryRepository;
use App\Repository\CguRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @package App\Controller
 */
class CguController extends AbstractController
{
    /**
     * @Route("/admin/cgu/{id}", name="admin_cgu")
     * @return Response Permet d'ajouter des CGU à une categorie avec un fichier pdf
     */
    public function adminCgu(int $id, Request $request, EntityManagerInterface $entityManager,
                             SluggerInterface $slugger, CategoryRepository $categoryRepository){
        $category = $categoryRepository->find($id);

        $cgu = new Cgu();
        $cgu->setCategory($category);

        $form = $this->createForm(CguType::class, $cgu);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $cgu->setCategory($category);
            $pdfFile = $form->get('pdfFilename')->getData();

            if($pdfFile){
                //On recupere le nom original du pdf
                $originalFilename = pathinfo($pdfFile->getClientOriginalName(), PATHINFO_FILENAME);

                //On change son nom pour eviter toutes failles de securités
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$pdfFile->guessExtension();

                //On met le pdf dans un dossier du projet, c'est juste le nom
                //du pdf qui est en BDD
                try{
                    $pdfFile->move(
                        $this->getParameter('pdf_directory'), $newFilename
                    );
                }catch (FileException $e){

                }

                $cgu->setPdfFilename($newFilename);
            }



            $entityManager->persist($cgu);
            $entityManager->flush();



            $this->addFlash('success', "cgu.added");

            return $this->redirectToRoute('category_edit', ['category' => $id]);
        }

        return $this->render('cgu/admin-cgu.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @IsGranted("ROLE_USER") On recois un lien vers cette route en cas d'acceptation d'une demande de categorie,
     * indispensable d'etre connecté pour verifier si on a signé les cgu ou pas.
     * @Route("/cgu/{id}", name="show_cgu")
     * @return Response Permet d'afficher une CGU sous forme de pdf, avec possibilité de la signer
     * attention, c'est l'Id de la categorie à qui appartient la cgu, pas celui de la cgu!
     */
    public function showCgu(int $id, CguRepository $cguRepository){
        //$cgu = $cguRepository->findOneBy(['category' => $id]);
        $cgus = $cguRepository->findBy(['category' => $id]);


        return $this->render('cgu/show-cgu.html.twig', [
            'cgus' => $cgus
        ]);
    }


    /**
     * @Route("/cgu/delete/{id}", name="delete_cgu")
     */
    public function deleteCgu(int $id,
                              CategoryRepository $categoryRepository,
                              CguRepository $cguRepository,
                              EntityManagerInterface $entityManager){
        //On récupère la cgu en question
        $cgu = $cguRepository->find($id);
        //On récupère aussi la categorie relié, pour pouvoir lui enlever la cgu
        $category = $categoryRepository->find($cgu->getCategory()->getId());
        $category->removeCgu($cgu);
        //Mise à jour de la category en BDD
        $entityManager->persist($category);

        $entityManager->remove($cgu);
        $entityManager->flush();
        unlink($this->getParameter('pdf_directory').'/'.$cgu->getPdfFilename());

        $this->addFlash("success", "edit.success");

        return $this->redirectToRoute('category_edit', ['category' => $category->getId()]);
    }


    /**
     * @Route("/cgu/accept/{id}", name="accept_cgu")
     */
    public function acceptCgu(int $id, EntityManagerInterface $entityManager, CguRepository $cguRepository){
        $cgu = $cguRepository->find($id);
        $user = $this->getUser();

        $user->addCguAccepted($cgu);
        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash('success',"cgu.accepted");

        return $this->redirectToRoute('show_cgu', ['id' => $cgu->getCategory()->getId()]);
    }


    /**
     * @Route("/admin/cgu/read/{id}", name="admin_read_cgu")
     * Permet l'affichage correct des CGU si elles sont en markdown
     */
    public function readCguMdAdmin(int $id, CguRepository $cguRepository){
        $cgu = $cguRepository->find($id);

        $cguMd = file_get_contents('pdf/upload/cgu/'.$cgu->getPdfFilename());
        return $this->render("cgu/readMdAdmin.html.twig", [
            'cguMd' => $cguMd
        ]);


    }

    /**
     * @Route("/cgu/read/{id}", name="read_cgu")
     * Permet l'affichage correct des CGU si elles sont en markdown
     */
    public function readCguMdPublic(int $id, CguRepository $cguRepository){
        $cgu = $cguRepository->find($id);

        $cguMd = file_get_contents('pdf/upload/cgu/'.$cgu->getPdfFilename());
        return $this->render("cgu/readMdPublic.html.twig", [
            'cguMd' => $cguMd
        ]);


    }
}
