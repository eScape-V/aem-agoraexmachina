<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAdmins()
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQueryBuilder();
        $query
            ->select('u')
            ->from('App:User','u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles','%"ROLE_ADMIN"%');
        return $query->getQuery()->getResult();
    }

    public function findAdminsRestreint()
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQueryBuilder();
        $query
            ->select('u')
            ->from('App:User','u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles','%"ROLE_ADMIN_RESTRICTED"%');
        return $query->getQuery()->getResult();
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }


    public function findUsersInWorkshop( $workshop): array
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.proposals', 'p')
            ->innerJoin('u.workshops', 'w')
            ->where('p.workshop = :workshop')
            ->setParameter('workshop', $workshop)
            ->getQuery()
            ->getResult()
            ;
    }


    //$limit => le nombre de résultat par page
    //$page => le numero de la page où on est
    public function findAllWithPagination($page, $limit){
        return $this->createQueryBuilder('u')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    //getSingleScalarResult permet de retourner non plus un tableau de résultat,
    //mais juste un entier avec le nombre de lignes
    public function getTotalUsers(){
        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    //Permet de recuperer l'utilisateur Anonyme en BDD
   /* public function findAnonUser(){
        return $this->createQueryBuilder('u')
            ->where('u.username = :name')
            ->setParameter('name', 'AnonymeAgoraExMachina')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }*/





//    public function findAllUsersByCategory($category)
//    {
//        $entityManager = $this->getEntityManager();
//        $query = $entityManager->createQueryBuilder();
//        $query
//            ->select('u')
//            ->from('App:Category', 'c')
//            ->where('c.users = id')
//            ->setParameter('category', $category);
//        return $query->getQuery()->getResult();
//    }

//    public function findAllUsersByCategory($id)
//    {
//        $entityManager = $this->getEntityManager();
//
//        $query = $entityManager->createQueryBuilder();
//        $query
//            ->select('u.id')
//            ->from('App:Category', 'c')
//            ->join('c.users','u')
//            ->andWhere('c.id = :id')
//            ->setParameter('id', $id)
//            ;
//        return  $query->getQuery()->getResult();
//    }
//    /**
//     * @param Category $category
//     * @return \Doctrine\ORM\QueryBuilder
//     */
//   public function findAllUsersByCategory (Category $category): \Doctrine\ORM\QueryBuilder
//   {
//
//  $qb = $this->createQueryBuilder('u');
//
//      $qb -> where('u.categories = category') -> setParameter('category', $category);
//      return $qb;
//    }
}
