<?php

namespace App\Repository;

use App\Entity\Theme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Theme|null find($id, $lockMode = null, $lockVersion = null)
 * @method Theme|null findOneBy(array $criteria, array $orderBy = null)
 * @method Theme[]    findAll()
 * @method Theme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThemeRepository extends ServiceEntityRepository
{

    /**
     * Cette fonction permet de recuperer tous les themes et leurs associations, pour limiter le nombre de queries a
     * la BDD
     * @return int|mixed|string
     */
    public function findAllThemes()
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQueryBuilder();
        $query
            ->select('t','c','w','p','f')
            ->from('App\Entity\Theme', 't')
            ->leftJoin('t.category','c')
            ->leftJoin('t.workshops','w')
            ->leftJoin('w.proposals','p')
            ->leftJoin('p.forums','f')
            ->orderBy('c.name')
            ;
        dump($query);
        return $query->getQuery()->getResult();
    }

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Theme::class);
    }


    public function findThemesByUser( $id ): array
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.category', 'c')
            ->innerJoin('c.users', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
            ;
    }

    //$limit => le nombre de résultat par page
    //$page => le numero de la page où on est
    public function findAllWithPagination($page, $limit){
        return $this->createQueryBuilder('t')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    //getSingleScalarResult permet de retourner non plus un tableau de résultat,
    //mais juste un entier avec le nombre de lignes
    public function getTotalThemes(){
        return $this->createQueryBuilder('t')
            ->select('COUNT(t)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    //Permet de savoir combien de copies ont été faite,
    //pour nommer correctement la nouvelle
    public function getCopyCount($themeName){
        return $this->createQueryBuilder('t')
            ->select('COUNT(t)')
            ->andWhere('t.name LIKE :themeName')
            ->setParameter('themeName', $themeName.'%')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
