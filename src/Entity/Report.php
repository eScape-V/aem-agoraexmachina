<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Les signalements sont crées quand un moderateur signale un forum.
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int L'identifiant dans la BDD
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface La date du signalement
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reports",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @var User L'utilisateur concerné par le signalement
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Forum::class, inversedBy="reports",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @var Forum Le forum entrainant le signalement
     */
    private $forum;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reported")
     * @ORM\JoinColumn(nullable=false)
     * L'utilisateur qui à fait le signalement
     */
    private $reporter;

    /**
     * @ORM\Column(type="boolean")
     * Pour savoir si la demande est traitée
     */
    private $isDone;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="report")
     * Les notifications liées à ce signalement
     */
    private $notifications;

    /**
     * @ORM\ManyToOne(targetEntity=Proposal::class, inversedBy="reports")
     * La proposition qui a été signalée
     */
    private $proposal;

    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getForum(): ?Forum
    {
        return $this->forum;
    }

    public function setForum(?Forum $forum): self
    {
        $this->forum = $forum;

        return $this;
    }

    public function getReporter(): ?User
    {
        return $this->reporter;
    }

    public function setReporter(?User $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setReport($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getReport() === $this) {
                $notification->setReport(null);
            }
        }

        return $this;
    }

    public function getProposal(): ?Proposal
    {
        return $this->proposal;
    }

    public function setProposal(?Proposal $proposal): self
    {
        $this->proposal = $proposal;

        return $this;
    }
}
