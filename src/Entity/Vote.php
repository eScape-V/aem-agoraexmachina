<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Les votes se font sur les propositions par les utilisateurs. Ils peuvent être pour, contre ou neutre pour les votes à 3 niveaux
 * Les votes à 5 niveaux peuvent prendre les valeurs totalement d"accord, d"accord, mitigé, pas d"accord, désaccord total
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 */
class Vote
{
	public function __construct()
                                                      	{
                                                      		$this->creationDate = new \DateTime('now');
                                                      	}
	
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int L'identifiant dans la BDD
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proposal", inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     * @var Proposal La proposition concernée par le vote
     */
    private $proposal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     * @var User L'utilisateur votant
     */
	private $user;

    /**
     * @ORM\Column(type="date")
     * @var \DateTimeInterface La date du vote
     */
    private $creationDate;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": "0"})
     * Vote à 3 niveaux
     */
    private $votedFor;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": "0"})
     * Vote à 3 niveaux
     */
    private $votedAgainst;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": "0"})
     * Vote à 3 niveaux
     */
    private $votedBlank;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * Vote à 5 niveaux
     */
    private $votedFullAgreement;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * Vote à 5 niveaux
     */
    private $votedAgree;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * Vote à 5 niveaux
     */
    private $votedMixed;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * Vote à 5 niveaux
     */
    private $votedDisagree;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * Vote à 5 niveaux
     */
    private $votedTotalDisagreement;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $votedPoints;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProposal(): ?Proposal
    {
        return $this->proposal;
    }

    public function setProposal(?Proposal $proposal): self
    {
        $this->proposal = $proposal;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getVotedFor(): ?bool
    {
        return $this->votedFor;
    }

    public function setVotedFor(?bool $votedFor): self
    {
        $this->votedFor = $votedFor;

        return $this;
    }

    public function getVotedAgainst(): ?bool
    {
        return $this->votedAgainst;
    }

    public function setVotedAgainst(?bool $votedAgainst): self
    {
        $this->votedAgainst = $votedAgainst;

        return $this;
    }

    public function getVotedBlank(): ?bool
    {
        return $this->votedBlank;
    }

    public function setVotedBlank(?bool $votedBlank): self
    {
        $this->votedBlank = $votedBlank;

        return $this;
    }

    public function getVotedFullAgreement(): ?bool
    {
        return $this->votedFullAgreement;
    }

    public function setVotedFullAgreement(?bool $votedFullAgreement): self
    {
        $this->votedFullAgreement = $votedFullAgreement;

        return $this;
    }

    public function getVotedAgree(): ?bool
    {
        return $this->votedAgree;
    }

    public function setVotedAgree(?bool $votedAgree): self
    {
        $this->votedAgree = $votedAgree;

        return $this;
    }

    public function getVotedMixed(): ?bool
    {
        return $this->votedMixed;
    }

    public function setVotedMixed(?bool $votedMixed): self
    {
        $this->votedMixed = $votedMixed;

        return $this;
    }

    public function getVotedDisagree(): ?bool
    {
        return $this->votedDisagree;
    }

    public function setVotedDisagree(?bool $votedDisagree): self
    {
        $this->votedDisagree = $votedDisagree;

        return $this;
    }

    public function getVotedTotalDisagreement(): ?bool
    {
        return $this->votedTotalDisagreement;
    }

    public function setVotedTotalDisagreement(?bool $votedTotalDisagreement): self
    {
        $this->votedTotalDisagreement = $votedTotalDisagreement;

        return $this;
    }

    public function getVotedPoints(): ?int
    {
        return $this->votedPoints;
    }

    public function setVotedPoints(?int $votedPoints): self
    {
        $this->votedPoints = $votedPoints;

        return $this;
    }
}
