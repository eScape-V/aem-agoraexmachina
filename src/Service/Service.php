<?php

namespace App\Service;

use App\Entity\Theme;
use App\Entity\User;
use App\Entity\Workshop;
use App\Repository\CategoryRepository;
use App\Repository\NotificationRepository;
use App\Repository\ThemeRepository;
use App\Repository\UserRepository;
use App\Repository\WebsiteRepository;
use App\Repository\WorkshopRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Contracts\Translation\TranslatorInterface;

class Service
{
    protected $userRepository;
    protected $websiteRepository;
    protected $workshopRepository;
    protected $themeRepository;
    protected $categoryRepository;
    protected $notificationRepository;
    protected $entityManager;
    protected $translator;
    protected $session;


    public function __construct(WebsiteRepository $websiteRepository, WorkshopRepository $workshopRepository
                                , CategoryRepository $categoryRepository, EntityManagerInterface $entityManager
                                , NotificationRepository $notificationRepository, TranslatorInterface $translator
                                , UserRepository $userRepository, ThemeRepository $themeRepository){

        $this->websiteRepository = $websiteRepository;
        $this->workshopRepository = $workshopRepository;
        $this->categoryRepository = $categoryRepository;
        $this->entityManager = $entityManager;
        $this->notificationRepository = $notificationRepository;
        $this->userRepository = $userRepository;
        $this->themeRepository = $themeRepository;

        $this->translator = $translator;

    }

    //retourne la couleur choisie par l'admin
    public function getWebsiteColor(){
        return $this->websiteRepository->find(1)->getBackgroundColor();
    }

    //retourne si le site doit etre indexer ou non
    public function isIndexing(){
        return $this->websiteRepository->find(1)->getIndexing();
    }

    //Envoie un notif au debut de la période de vote, et à la fin de celle ci
    public function dateVerification(){

        $workshops = $this->workshopRepository->findAll();
        $today = new \DateTime();
        foreach($workshops as $workshop){

            //On recupere que les utilisateurs qui sont inscrit dans une categorie qui contient des ateliers
            $users = $this->userRepository->findUsersInWorkshop($workshop);

            //Si la période de discussion est finis, on déban tous ceux qui le sont
            /*if ($workshop->getDateEnd()->format('Y-m-d') > $today->format('Y-m-d')){
                foreach ($workshop->getBannedUsers() as $bannedUser){
                    $workshop->removeBannedUser($bannedUser);
                    $this->entityManager->persist($workshop);
                }
            }*/

            foreach ($users as $user){


                //Si la date de debut de vote d'un atelier est aujourd'hui, et que l'utilisateur n'a pas deja une notification
                //à ce propos, on lui en envoie une
                if ($workshop->getDateVoteBegin()->format('Y-m-d') == $today->format('Y-m-d')){
                   if($this->notificationRepository->verificationAlreadySend($user, $this->translator->trans('vote.notification.begin').$workshop->getName()) == null){
                        $notification = $user->prepareNotification($this->translator->trans('vote.notification.begin').$workshop->getName());
                        $this->entityManager->persist($notification);
                    }
                }

                //Si la date de fin de vote d'un atelier est demain, et que l'utilisateur n'a pas deja une notification
                //à ce propos, on lui en envoie une
                $tomorrow = new \DateTime("tomorrow");
                if($workshop->getDateVoteEnd()->format('Y-m-d') == $tomorrow->format('Y-m-d')){
                    if($this->notificationRepository->verificationAlreadySend($user, $this->translator->trans('vote.notification.end').$workshop->getName()) == null){
                        $notification = $user->prepareNotification($this->translator->trans('vote.notification.end').$workshop->getName());
                        $this->entityManager->persist($notification);
                    }
                }
            }
        }
        $this->entityManager->flush();
    }


    //Permet de copier un Atelier, utile ici pour pouvoir l'appeler depuis plusieurs endroits
    //(notamment de ThemeController)
    public function copyWorkshop(Workshop $workshop, User $user): Workshop
    {
        $workshopCopy = new Workshop();
        $i = $this->workshopRepository->getCopyCount($workshop->getName());

        $workshopCopy->setTheme($workshop->getTheme());
        $workshopCopy->setUser($user);

        //Evite les erreurs si le nom original est trop long pour concaténer (limite de 50)
        if (strlen($workshop->getName()) <= 42){
            $workshopCopy->setName($workshop->getName().' Copie '.$i);
        }elseif (strlen($workshop->getName()) <= 48){
            $workshopCopy->setName($workshop->getName().' '.$i);
        }else{
            $workshopCopy->setName(substr($workshop->getName(), 0, 48).' '.$i);
        }

        $workshopCopy->setDescription($workshop->getDescription());
        $workshopCopy->setDateBegin($workshop->getDateBegin());
        $workshopCopy->setDateEnd($workshop->getDateEnd());
        $workshopCopy->setRightsSeeWorkshop($workshop->getRightsSeeWorkshop());
        $workshopCopy->setRightsVoteProposals($workshop->getRightsVoteProposals());
        $workshopCopy->setRightsWriteProposals($workshop->getRightsWriteProposals());
        $workshopCopy->setQuorumRequired($workshop->getQuorumRequired());
        $workshopCopy->setRightsDelegation($workshop->getRightsDelegation());
        $workshopCopy->setDateVoteBegin($workshop->getDateVoteBegin());
        $workshopCopy->setDateVoteEnd($workshop->getDateVoteEnd());
        if ($workshop->getImage() != null){
            $workshopCopy->setImage($workshop->getImage());
        }
        if ($workshop->getDelegationDeepness() != null){
            $workshopCopy->setDelegationDeepness($workshop->getDelegationDeepness());
        }
        if ($workshop->getKeytext() != null){
            $workshopCopy->setKeytext($workshop->getKeytext());
        }

        return $workshopCopy;
    }



    //Permet de créer un utilisateur "Anonyme", qui remplace tout les utilisateurs effacés
    //(devient l'auteur de leurs themes crées, propositions, ect...)
   /* public function createAnonUser(): User
    {
        $anon = new User();
        $anon->setUsername('AnonymeAgoraExMachina');
        $anon->setEmail('anonymeAgoraExMachina@mail.fr');
        $anon->setPassword('$argon2id$v=19$m=65536,t=4,p=1$U9lXU5il/M4uf0dwfSg4Ew$3WsYWqa8Fp/GRmFXI8vsCrpCSWFROPqxBH9wAqqbciI'); //AnonymousUser0+
        $anon->setIsAllowedEmails(false);
        $anon->setFirstName('Anon');
        $anon->setLastName('Nymous');

        $this->entityManager->persist($anon);
        $this->entityManager->flush();

        return $anon;

    }*/


}